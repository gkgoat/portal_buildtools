package poor

import "crypto/sha256"

func Hash(a, b []byte) []byte {
	s := sha256.New()
	s.Write(a)
	return s.Sum(b)
}

func Mine(f func([]byte) []byte) ([]byte, []byte) {
	v := Hash([]byte{}, []byte{})
	for {
		fv := f(v)
		fh := Hash(fv, v)
		if fh[0] == 0 && fh[1] == 0 && fh[2] == 0 {
			return v, fv
		}
		v = fh
	}
}
