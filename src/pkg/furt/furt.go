package furt

import (
	"context"
	"net"
	"net/http"
	"unsafe"

	"github.com/prologic/httpfs/fsapi"
	"golang.org/x/build/revdial"
	"nhooyr.io/websocket"
)

func NewWsListener(u string) (*revdial.Listener, error) {
	w, err := websocket.Dial(context.Background(), u, nil)
	if err != nil {
		return nil, err
	}
	n := websocket.NetConn(context.Background(), w, websocket.MessageBinary)
	l := revdial.NewListener(n, func(c context.Context) (net.Conn, error) {
		w, err := websocket.Dial(c, u, nil)
		if err != nil {
			return nil, err
		}
		n := websocket.NetConn(c, w, websocket.MessageBinary)
		return n, nil
	})
	return l, nil
}
func NewWsDialer(c *websocket.Conn) *revdial.Dialer {
	return revdial.NewDialer(websocket.NetConn(context.Background(), c, websocket.MessageBinary), "/")
}
func ServeFiles(u string, dir string) {
	w, err := NewWsListener(u)
	if err != nil {
		panic(err)
	}
	go http.Serve(w, http.FileServer(http.Dir(dir)))
}
func CreateClient(d net.Dialer) *fsapi.Client {
	c := fsapi.NewClient("", false)
	u := unsafe.Pointer(c)
	u = unsafe.Pointer(uintptr(u) + unsafe.Sizeof(""))
	s := (**http.Client)(u)
	*s = &http.Client{Transport: &http.Transport{Dial: d.Dial}}
	return c
}
