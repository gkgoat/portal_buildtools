package json_graph

import (
	"fmt"
	"strings"
)

// JSONGraph is an alternate representation of our build graph; will contain different information
// to the structures in core (also those ones can't be printed as JSON directly).
type JSONGraph struct {
	Packages map[string]JSONPackage `json:"packages"`
	Subrepos map[string]*JSONGraph  `json:"subrepos,omitempty"`
}

// JSONPackage is an alternate representation of a build package
type JSONPackage struct {
	name    string
	subrepo string
	Targets map[string]JSONTarget `json:"targets"`
}

// JSONTarget is an alternate representation of a build target
type JSONTarget struct {
	Inputs      []string    `json:"inputs,omitempty" note:"declared inputs of target"`
	Outputs     []string    `json:"outs,omitempty" note:"corresponds to outs in rule declaration"`
	GraphOutput *string     `json:"graph_out"`
	Sources     interface{} `json:"srcs,omitempty" note:"corresponds to srcs in rule declaration"`
	Tools       interface{} `json:"tools,omitempty" note:"corresponds to tools in rule declaration"`
	Deps        []string    `json:"deps,omitempty" note:"corresponds to deps in rule declaration"`
	Data        []string    `json:"data,omitempty" note:"corresponds to data in rule declaration"`
	Labels      []string    `json:"labels,omitempty" note:"corresponds to labels in rule declaration"`
	Requires    []string    `json:"requires,omitempty" note:"corresponds to requires in rule declaration"`
	Command     string      `json:"command,omitempty" note:"the currently active command of the target. not present on filegroup or remote_file actions"`
	Hash        string      `json:"hash" note:"partial hash of target, does not include source hash"`
	Test        bool        `json:"test,omitempty" note:"true if target is a test"`
	Binary      bool        `json:"binary,omitempty" note:"true if target is a binary"`
	TestOnly    bool        `json:"test_only,omitempty" note:"true if target should be restricted to test code"`
}

type JSONLabel struct {
	Package string
	Extra   string
	Subrepo string
	Tgt     string
}

func (g *JSONGraph) At(l JSONLabel) JSONTarget {
	if l.Subrepo != "" {
		g = g.Subrepos[l.Subrepo]
	}
	return g.Packages["["+l.Extra+"]"+l.Package].Targets[l.Tgt]
}

func braces(x string) (string, string) {
	z := ""
	for {
		w := x[0]
		x = x[1:]
		if w == '[' {
			a, b := braces(x)
			x = b
			z += fmt.Sprintf("[%s]", a)
		}
		if w == ']' {
			return z, x
		}
	}
}
func ParseJSONLabel(x string) JSONLabel {
	y := x
	e := ""
	if len(y) != 0 && y[0] == '[' {
		e, y = braces(y[1:])
	}
	w := strings.SplitN(x, "//", 2)
	b := w[0]
	a := ""
	if len(b) != 0 {
		a = b[1:]
		b = w[1]
	}
	ww := strings.SplitN(b, ":", 2)
	return JSONLabel{Package: ww[0], Tgt: ww[1], Extra: e, Subrepo: a}
}

func (j JSONLabel) String() string {
	return "[" + j.Extra + "]@" + j.Subrepo + "//" + j.Package + ":" + j.Tgt
}
