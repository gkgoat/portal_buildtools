package cone

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"time"

	"github.com/awgh/ratnet/api"
	"golang.org/x/build/revdial"
)

type Cone struct {
	node    *api.Node
	readId  string
	writeId string
	Done    chan struct{}
}

func NewCones(n *api.Node, ida string, idb string) (Cone, Cone) {
	var a, b Cone
	a.node = n
	b.node = n
	a.readId = idb
	a.writeId = ida
	b.readId = ida
	b.writeId = idb
	a.Done = make(chan struct{})
	b.Done = make(chan struct{})
	return a, b
}

func NewCone(n *api.Node, r, w string) Cone {
	return Cone{node: n, readId: r, writeId: w, Done: make(chan struct{})}
}

func (c *Cone) Bud() (Cone, Cone) {
	return NewCones(c.node, c.writeId, c.readId)
}

func (c Cone) Ids() (string, string) {
	return c.writeId, c.readId
}

type Msg struct {
	Id      string
	Content []byte
}

func (c *Cone) Write(x []byte) (int, error) {
	y := Msg{Id: c.writeId, Content: x}
	b, err := json.Marshal(y)
	if err != nil {
		return 0, err
	}
	ch := make(chan struct{})
	go func() {
		c.node.Out() <- &api.Msg{Name: "__all__", Content: bytes.NewBuffer(b)}
		ch <- struct{}{}
	}()
	go func() {
		x := <-c.Done
		ch <- x
		c.Done <- x
	}()
	_ = <-ch
	return len(x), nil
}

func (c *Cone) Read(x []byte) (int, error) {
	for {
		select {
		case i := <-c.node.In():
			r, _ := ioutil.ReadAll(i.Content)
			var y Msg
			err := json.Unmarshal(r, &y)
			if err == nil && y.Id == c.readId {
				b := bytes.NewBuffer(y.Content)
				return b.Read(x)
			} else {
				defer func() {
					go func() {
						c.node.In() <- i
					}()
				}()
			}
		case x := <-c.Done:
			go func() { c.Done <- x }()
			return 0, fmt.Errorf("Cancelled")
		}
	}
}

func (c *Cone) Close() {
	c.Done <- struct{}{}
}

func (c *Cone) LocalAddr() (a net.Addr) {
	return
}

func (c *Cone) RemoteAddr() (a net.Addr) {
	return
}

func (c *Cone) SetDeadline(_ time.Time) error {
	return fmt.Errorf("not supported")
}

func (c *Cone) SetReadDeadline(_ time.Time) error {
	return fmt.Errorf("not supported")
}

func (c *Cone) SetWriteDeadline(_ time.Time) error {
	return fmt.Errorf("not supported")
}

func (c *Cone) Listen() net.Listener {
	return revdial.NewListener(c, func(c context.Context) (net.Conn, error) {
		return nil, fmt.Errorf("not supported")
	})
}

func (c *Cone) Get() net.Dialer {
	return revdial.NewDialer(c)
}
