#include <memory>
template<typename O> struct ObjC{
    inline void operator ()(O *x){
        [x dealloc];
    }
};
struct Platform;
@interface PPPlatform: NSObject{
    @public std::shared_ptr<Platform> cc;
}
@end
struct Platform{
    std::shared_ptr<PPPlatform,ObjC<PPPlatform>> oc = [PPlatform alloc];
    Platform(){
        oc.cc = this;
    }
};